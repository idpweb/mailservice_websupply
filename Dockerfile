FROM python:3.11-rc-bullseye

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

COPY *.py /usr/src/app/

CMD ["python3", "/usr/src/app/mail_service.py"]
