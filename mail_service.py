import smtplib, ssl
import pika
import time
import json

time.sleep(10)

def send_email(receiver_email, message):
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    sender_email = "websupply.idpweb@gmail.com"
    password = "danimocanuregele"

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message)


print("Mail service started", flush=True)

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='rabbitmq'))
channel = connection.channel()
channel.queue_declare(queue='email_queue', durable=True)

print("Mail service connected", flush=True)


def callback(ch, method, properties, body):
    data_json = json.loads(body.decode())
    print(data_json, flush=True)
    if "receiver" not in data_json:
        print("Field 'receiver' is not present")
        return
    if "message" not in data_json:
        print("Field 'message' is not present")
        return
    receiver_email = data_json["receiver"]
    message = data_json["message"]
    send_email(receiver_email, message)
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='email_queue', on_message_callback=callback)
channel.start_consuming()


